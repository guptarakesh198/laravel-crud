<!DOCTYPE html>
<html lang="en">
<head>
  <title>{{ $title or 'Learning Laravel' }}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
  
</head>
<body>

<div class="container" id="app">
<div class="row">

@include('flash-message')

@yield('validation_error')

@yield('welcome')

@yield('content')

@yield('studentlist')

</div>

@yield('vuemodel')

</div>

</body>
<script src="{{ asset('public/js/jquery.min.js') }}"></script>
<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/js/vue.js') }}"></script>
<script src="{{ asset('public/js/axios.min.js') }}"></script>

@yield('vuescript')

</html>