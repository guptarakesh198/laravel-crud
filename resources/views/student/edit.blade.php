@extends('layout.master')

@section('content')

 <div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 30px;">

  <h2>Student Form</h2>

  <form action="{{ url('/studentupdate/'.$data->id) }}" method="post">

  	{{ csrf_field() }}

    <div class="form-group">
      <label for="name">Name :</label>
      <input type="text" class="form-control" id="name" value="{{ $data->sname }}" placeholder="Enter name" name="sname" required="">
    </div>

    <div class="form-group">
      <label for="std_class">Std :</label>
      <input type="text" class="form-control" id="std_class" value="{{ $data->std_class }}" placeholder="Enter Std" name="std_class" required="">
    </div>    

    <button type="submit" class="btn btn-default">Update</button>

  </form>

  </div>

@endsection