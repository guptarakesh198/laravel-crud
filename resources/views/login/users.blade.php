@extends('layout.master')


@section('welcome')
  
  <div class="col-lg-6 col-lg-offset-3" style="margin-top: 30px;">
      <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            
            Hi, @if(Session::has('username'))
                  {{ Session::get('username') }}
                @endif

        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            
              <a href="{{ url('/logout') }}" class="btn btn-danger btn-xs pull-right">Logout</a>
              <button class="btn btn-info btn-xs pull-right" data-toggle="modal" data-target="#addusernewform" style="margin-right:10px">Add New User</button>

        </div>

      </div>
  </div>

@endsection

@section('studentlist')

<div class="col-lg-6 col-lg-offset-3" style="margin-top:30px;">
<table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th>No</th>
        <th>Username</th>
        <th>Password</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="u in users">
          <td>@{{ u.id }}</td>
          <td>@{{ u.username }}</td>
          <td>@{{ u.password }}</td>
          <td>
            <a href="#" class="btn btn-xs btn-info" @click="showupdatemodel(u.id)"> Edit </a>
            <a href="#" class="btn btn-xs btn-danger" @click="deleteuser(u.id)"> Delete </a>
          </td>
      </tr>
    </tbody>
  </table>

  <nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
      <li class="page-item" v-for="n in pagecount"><a class="page-link" href="#" @click="activepage(n)">@{{ n }}</a></li>
    </ul>
  </nav>



</div>

@endsection

@section('vuemodel')

<!-- add new Modal -->
<div class="modal fade" id="addusernewform" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">
                   Add New User
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form class="form-horizontal">
            
                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="username">Username</label>
                    <div class="col-sm-10">
                        <input type="text" name="addusername" class="form-control" id="addusername" v-model="addusername" placeholder="Enter Username"/>
                    </div>
                  </div>
            
                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="password" >Password</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="addpassword" v-model="addpassword" placeholder="Enter Password"/>
                    </div>
                  </div>
            
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <a class="btn btn-default" @click="addRecord">Add User</a>
                    </div>
                  </div>
                </form>
            
            </div>
            

        </div>
    </div>
</div>



<!-- Update new Modal -->
<div class="modal fade" id="updateuserform" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">
                   Update User
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                
                <form class="form-horizontal">
                
                  <input type="hidden" name="updateid" v-model="updateid" value="">

                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="updateusername">Username</label>
                    <div class="col-sm-10">
                        <input type="text" name="updateusername" class="form-control" id="updateusername" v-model="updateusername" placeholder="Enter Username"/>
                    </div>
                  </div>
            
                  <div class="form-group">
                    <label class="col-sm-2 control-label"
                          for="updatepassword" >Password</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="updatepassword" v-model="updatepassword" placeholder="Enter Password"/>
                    </div>
                  </div>
            
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <a class="btn btn-default" @click="updateRecord">Update User</a>
                    </div>
                  </div>
                </form>
            
            </div>
            

        </div>
    </div>
</div>



@endsection


@section('vuescript')


<script>

var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    tURL : '{{ url('/getallusers') }}',
    totalURL : '{{ url('/countusers') }}',
    addURL : '{{ url('/adduser') }}',
    getURL : '{{ url('/getuser') }}',
    updateURL : '{{ url('/updateuser') }}',
    deleteURL : '{{ url('/deleteuser') }}',
    addusername : '',
    addpassword : '',
    updateid : '',
    updateusername : '',
    updatepassword : '',
    pagecount : 0,
    totalrecords : 0,
    currentpage : 0,
    listperpage : 5,
    users : []
  },
  mounted(){

   axios.get(this.totalURL)
        .then(function (response){
            
            app.totalrecords = response.data.total;
            app.pagecount = Math.ceil(app.totalrecords/app.listperpage);
            console.log(app.totalrecords +' = '+ app.listperpage +' = '+ app.pagecount);
            app.activepage(1);

          })
        .catch(function(error){
            console.log(error);
         });

  },
 watch: {
    // whenever question changes, this function will run
    pagecount: function (newvalue, oldvalue) {
       app.pagecount = newvalue;
    }

  },
  methods:{
    refreshRecords : function(){

      axios.get(this.totalURL)
        .then(function (response){
            
            app.totalrecords = response.data.total;
            app.pagecount = Math.ceil(app.totalrecords/app.listperpage);
            app.activepage(app.currentpage);

          })
        .catch(function(error){
            console.log(error);
         });

    },
    activepage: function(page){

       app.currentpage = page;
       app.getusers();

    },
    getusers: function(){

        app.users = [];
        axios.get(this.tURL+'/'+ app.currentpage + '/' + app.listperpage)
          .then(function (response){
              for(var i = 0; i < response.data.length; i++) {
                app.users.push(response.data[i]);
              }

            })
          .catch(function(error){
              console.log(error);
           });

    },
    addRecord: function(){
        
        if(this.addusername == ''){

            alert('Invalid Username');
            $('#addusername').focus();

        }else if(this.addpassword == ''){

            alert('Invalid Password');
            $('#addpassword').focus();

        }else{

                axios.post(this.addURL,{
                      username : this.addusername,
                      password : this.addpassword
                    })
                    .then(function (response){
                        if(response.data.sucess == 'yes'){

                            //app.users.push({'id':response.data.sucess.id,'username':this.addusername._value,'password':this.addpassword._value});
                            app.refreshRecords();
                      
                        }
                        $('#addusernewform').modal('hide');

                     })
                    .catch(function(error){
                        console.log(error);
                     });

        }
    },
    updateRecord: function(){
        
        if(this.updateusername == ''){

            alert('Invalid Username');
            $('#updateusername').focus();

        }else if(this.updatepassword == ''){

            alert('Invalid Password');
            $('#updatepassword').focus();

        }else{

                axios.post(this.updateURL,{
                      id : this.updateid,
                      username : this.updateusername,
                      password : this.updatepassword
                    })
                    .then(function (response){

                        if(response.data.sucess == 'yes'){
                           app.refreshRecords();                        
                        }
                        $('#updateuserform').modal('hide');

                     })
                    .catch(function(error){
                        console.log(error);
                     });

        }
    },
    deleteuser : function(id){

         axios.get(this.deleteURL+'/'+id)
          .then(function (response){
              
                if(response.data.sucess == 'yes'){
                      app.refreshRecords();                        
                  }

            })
          .catch(function(error){
              console.log(error);
           });

    },
    showupdatemodel: function(id){

      axios.get(this.getURL+'/'+id)
          .then(function (response){
              
                app.updateusername = response.data.username;
                app.updatepassword = response.data.password;
                app.updateid = id;
                $('#updateuserform').modal('show');

            })
          .catch(function(error){
              console.log(error);
           });


    }
  }
})

</script>

@endsection