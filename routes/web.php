<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/student','Studentcontroller@index');
Route::post('/studentadd','Studentcontroller@add');
Route::get('/studentedit/{id}','Studentcontroller@edit');
Route::post('/studentupdate/{id}','Studentcontroller@update');
Route::get('/studentdelete/{id}','Studentcontroller@delete');

Route::get('/signup/','SignupController@index');
Route::post('/sendmail','SignupController@send_email');

Route::get('/login/','LoginController@index');
Route::post('/login','LoginController@login');
Route::get('/users/','LoginController@users');
Route::get('/logout/','LoginController@logout');


Route::get('/getallusers/{page}/{recordsperpage}','ApiController@getallusers');
Route::post('/adduser','ApiController@adduser');
Route::get('/getuser/{id}','ApiController@getuser');
Route::post('/updateuser','ApiController@updateuser');
Route::get('/deleteuser/{id}','ApiController@deleteuser');
Route::get('/countusers/','ApiController@countusers');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
