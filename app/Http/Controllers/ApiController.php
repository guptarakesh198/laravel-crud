<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class ApiController extends Controller
{
    //

    public function getallusers($page,$recordsperpage = '5'){


    	$users = DB::table('users')
                ->offset(($page-1)*$recordsperpage)
                ->limit($recordsperpage)
                ->get()->toArray();;


    	//$users = DB::table('users')->get()->toArray();
    	echo json_encode($users);

    }

    public function getuser($id){

    	$data = DB::table('users')->where('id', $id)->first();
    	echo json_encode($data);

    }

    public function countusers(){

    	$count = DB::table('users')->count();
    	$out = array();
    	$out['total'] = $count;
    	echo json_encode($out);

    }

    public function adduser(Request $request){

    	$data = $request->all();
    	$out = array();
  		if($id = DB::table('users')->insertGetId($data)){
  			$out['sucess'] = 'yes';
  			$out['id'] = $id;
  		}else{
  			$out['sucess'] = 'no';
  		}

  		echo json_encode($out);

    }

    public function updateuser(Request $request){

    	$data = $request->all();
		$out = array();
		if(DB::table('users')->where('id',$data['id'])->update(['username' => $data['username'],'password' => $data['password']])){

			$out['sucess'] = 'yes';

		}else{

			$out['sucess'] = 'no';

		}

		echo json_encode($out);

    }

    public function deleteuser($id){

		$out = array();
    	if(DB::table('users')->where('id',$id)->delete()){
    		$out['sucess'] = 'yes';
    	}else{
    		$out['sucess'] = 'no';
    	}

    	echo json_encode($out);
    	
    }

}
