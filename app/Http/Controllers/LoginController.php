<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LoginController extends Controller
{
    //
    public function index(Request $request){

      if($request->session()->exists('login_status')) {
      		return redirect('users');
  		}
    	return view('login.index');

   	}

   	public function login(Request $request){

   		$data = $request->all();

		if(empty($data)){
			return redirect('/login');
		}

		$this->validate($request,['username'=>'required','password'=>'required']);

		$username = $data['username'];
		$password = $data['password'];

		if(DB::table('users')->where([['username','=',$username],['password','=',$password]])->exists()){

			 session(['login_status' => '1','username' => $username]);
			 return redirect('login')->with('success','Login Successfully');

		}else{

			return redirect('login')->with('error','Username and password not matched');

		}

   	}


   	public function users(Request $request){


   		//echo $request->session()->get('login_status');

   		if(!$request->session()->exists('login_status')) {
    		return redirect('login');
		}

   		//$users = DB::table('users')->get()->toArray();
   		return view('login.users');

   	}


   	public function logout(Request $request){

   		$request->session()->flush();
   		return redirect('login');
   	}

}
