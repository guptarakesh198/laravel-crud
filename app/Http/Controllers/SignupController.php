<?php

namespace App\Http\Controllers;

use App\Mail\SignupEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class SignupController extends Controller
{
    //
	public function index(){

		$title = 'Laravel Email Signup Form';
		return view('signup.index',['title'=>$title]);

	}

	public function send_email(Request $request){
		
		$data = $request->all();

		if(empty($data)){
			return redirect('/signup');
		}

		$this->validate($request,['name'=>'required','no'=>'required','email'=>'required']);

		$name = $data['name'];
		$no = $data['no'];
		$email = $data['email'];

		$objDemo = new \stdClass();
        $objDemo->name = $name;
        $objDemo->no = $no;
        $objDemo->email = $email;
        $objDemo->sender = 'Rakesh Gupta';
        $objDemo->receiver = $name;
 		
 		try{
        	Mail::to($email)->send(new SignupEmail($objDemo));
    	}
    	catch(\Exception $e){

    		return redirect('signup')->with('error', $e->getMessage());

		}

        return redirect('signup')->with('success','Send Message Successfully.');

	}

}
