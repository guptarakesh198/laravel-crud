<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Student;

class Studentcontroller extends Controller
{
    //
	public function index(){

		$title = 'Student';
		$student = new Student;
		$students = $student->all();

		return view('student.index',['title' => $title,'students' => $students]);
	}

	public function add(Request $request){

		$request->validate(['sname'=>'required','std_class'=>'required']);
		$data = $request->all();

		try{

		$student = new Student;
		$student->sname = ucwords($data['sname']);
		$student->std_class = $data['std_class'];
		$student->status = 1;
		$student->save();

		$request->session->flash('alert-success',"Student Add Successfully");

		}catch(\Exception $e){

			$request->session->flash('alert-error',$e->getMessage());
			return redirect()->back();

		}

		return redirect('student');

	}


	public function edit($id = ''){

		if($id == ''){
			return redirect()->back();
		}else{

			$student = Student::where('id',$id)->first();
			//$data = $student::find($id);
			return view('student.edit')->with(['data'=>$student]);
			
		}


	}

	public function update($id = '',Request $request){

		$request->validate(['sname'=>'required','std_class'=>'required']);
		$data = $request->all();
		try{

			Student::where('id',$id)->update(
    				['sname' => ucwords($data['sname']), 'std_class' => $data['std_class'], 'status'=>1]
				);

			$request->session()->flash('alert-success',"Student Update Successfully");

		}catch(\Exception $e){

			$request->session()->flash('alert-error',$e->getMessage());
			return redirect()->back();

		}

		return redirect('student');

	}

	public function delete($id='',Request $request){

		if($id != ''){

			try{

				Student::where('id',$id)->delete();
				$request->session()->flash('success',"Student Delete Successfully");

			}catch(\Exception $e){

				$request->session()->flash('error',$e->getMessage());
				return redirect()->back();

			}

			return redirect('student');

		}else{

			return redirect()->back();
		}

	}

}
